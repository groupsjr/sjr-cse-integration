<?php

namespace sjr\cse;

/**
*	gets site settings before handing building api request
*	@param string search query, unslashed
*	@param int page of results, with 10 per page
*	@param array extra parameters for api request https://developers.google.com/custom-search/json-api/v1/reference/cse/list
*		- q 
*		- siteSearch
*	@param int index of cx / key pair in wp-admin/options-general.php?page=google-cse-settings
*	@return object
*/
function api( $query = '', $page = 1, $params = NULL, $config_id = 0 ){
	$settings = get_settings();

	// @TODO should this default or return an error?
	$config_ids = array_keys( $settings->configs );
	if( !array_key_exists($config_id, $settings->configs) )
		$config_id = array_shift( $config_ids );

	$params = array_filter( wp_parse_args($params, array(
		'key' => $settings->configs[$config_id]['key'],
		'cx' => $settings->configs[$config_id]['cx'],
		'q' => $query,
		'num' => 10
	)) );

	$page = max( $page, 1 );
	$params['start'] = ( ($page - 1) * $params['num'] ) + 1;

	$data = api_request( $params ); 

	return $data;
}

/**
*	Builds the query that handles Google CS request
*	@param array https://developers.google.com/custom-search/json-api/v1/reference/cse/list
*	@return object
*/
function api_request( $params ){
	$params = array_filter( $params );

	$key = transient_key( 'cse-search', array($params, version()) );
	$response = get_transient( $key );

	if( !$response ){
		$url = 'https://www.googleapis.com/customsearch/v1?' . http_build_query( $params );
		$response = wp_remote_get( $url );

		set_transient( $key, $response, DAY_IN_SECONDS );
	}

	if( is_wp_error($response) ){
		$data = (object) array(
			'items' => array()
		);
	} else {
		$data = json_decode( $response['body'] );

		// google cse will return results starting at 1 when a page past the max is set
		if( !isset($data->items) || (!empty($data->queries) && ($data->queries->request[0]->startIndex < $params['start'])) ){
			//unset( $data->items );
			$data->items = array();
		}
	}

	return $data;
}

/**
*	creates value used for `cse:id` meta tag
*	@param int
*	@return string
*/
function generate_cse_id( $post_id ){
	$cse_id = uniqid();
	update_post_meta( $post_id, '_cse_id', $cse_id );
	
	return $cse_id;
}

/**
*	gets meta value used for `cse:id` meta tag
*	@param int
*	@return string
*/
function get_cse_id( $post_id ){
	$cse_id = get_post_meta( $post_id, '_cse_id', TRUE );
	
	return $cse_id;
}

/**
*	gets all pages of search results for a search query, filtered by \sjr\cse\get_results_all
*	@param string search query
*	@param array params for api request https://developers.google.com/custom-search/json-api/v1/reference/cse/list
*	@param int index of cx / key pair in wp-admin/options-general.php?page=google-cse-settings
*	@param int
*	@return array
*/
function get_results_all( $search_query, $params = NULL, $config_id = 0, $max_posts = 1000 ){
	$key = transient_key( 'cse-results-all', [$search_query, $params, $config_id, $max_posts, version()] );
	$posts = get_transient( $key );

	if( !is_array($posts) ){
		$api_page = 1;
		$posts = array();

		try{
			while( ($appended = get_results_raw($search_query, $api_page, $params, $config_id)) && (count($posts) <= $max_posts) ){
				$appended = apply_filters( '\sjr\cse\get_results_all', $appended );	
				$posts = array_merge( $posts, $appended );

				$api_page++;
			}
		} catch( \Exception $e ){
			if( !count($posts) )
				throw $e;
		}

		$posts = array_map( function($post){
			$post->_cached = 0;

			return $post;
		}, $posts );

		set_transient( $key, $posts, DAY_IN_SECONDS );
	} else {
		$posts = array_map( function($post){
			$post->_cached = 1;

			return $post;
		}, $posts );
	}
	
	return $posts;
}

/**
*	makes api request for single page of results
*	@param string
*	@param int page of results, with 10 per page
*	@param array 
*	@param int index of cx / key pair in wp-admin/options-general.php?page=google-cse-settings
*	@return array
*/
function get_results_raw( $search_query, $page = 1, $params = NULL, $config_id = 0 ){
	$key = transient_key( 'cse-raw', array($search_query, $page, $params, $config_id, version()) );
	$posts = get_transient( $key );

	if( !is_array($posts) ){
		try{
			$api = api( $search_query, $page, $params, $config_id );

			if( !empty($api->error->code) )
				throw new \Exception( sprintf('%s, %s', $api->error->errors[0]->message, $api->error->errors[0]->reason) );

			$posts = empty( $api->items ) ? array() : array_map( function($item){
				$post = (object) array(
					'cache_id' => isset($item->cacheId) ? $item->cacheId : '',
					'display_link' => $item->displayLink,
					'link' => $item->link,
					'title' => $item->title,
					'_raw' => $item
				);

				return $post;
			}, $api->items );

			set_transient( $key, $posts, DAY_IN_SECONDS );
		} catch( \Exception $e ){
			return array();
		}
	}

	return $posts;
}

/**
*	gets site settings found in /wp-admin/options-general.php?page=google-cse-settings
*	@return object
*/
function get_settings(){
	$defaults = array(
		'configs' => array(),

		'append' => 0,
		'replace' => 0,
		'version' => version()
	);
	
	$config = array(
		'cx' => '',
		'key' => '',
	);

	$settings = (object) array_merge( $defaults, (array) get_option('google-cse-settings') );
	$settings->configs = array_filter( $settings->configs, function($config){
		return !empty( array_filter($config) );
	} );
	
	if( !empty($settings->cx) && !empty($settings->key) ){
		// move legacy single cx and key into config array
		$config = array(
			'cx' => $settings->cx,
			'key' => $settings->key
		);

		unset( $settings->cx, $settings->key );

		array_push( $settings->configs, $config );
	} elseif( !count($settings->configs) ){
		// make sure at least blank default exists
		array_push( $settings->configs, $config );
	}

	$settings->configs = array_values( $settings->configs );

	return $settings;
}

/**
*	generateds transient identifier
*	@param string
*	@param array
*	@return string
*/
function transient_key( $prefix, array $vars ){
	$vars[] = version();

	$key = substr( $prefix.'-'.md5(serialize($vars)), 0, 40 );

	return $key;
}

/**
*	gets current version of plugin
*	@return string
*/
function version(){
	static $version;

	if( !$version ){
		$data = get_file_data( __DIR__.'/_plugin.php', array('Version'), 'plugin' );
		$version = $data[0];
	}

	return $version;
}

/**
*	echo cse:id meta tag in header
*	attached to wp_head action
*/
function wp_head(){
	if( !is_singular() )
		return;
	
	$post_id = get_the_ID();
	$cse_id = get_cse_id( $post_id ) ?: generate_cse_id( $post_id );
	
	echo sprintf( '<meta property="cse:id" content="%s" />', $cse_id );
}