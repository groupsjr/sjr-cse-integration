<?php
/*
Plugin Name:	Group SJR Google CSE Integration
Plugin URI:		
Description:	Requires WP > 4.4, SJR Core > 0.9.9d
Author:			
Text Domain:	
Domain Path:	/lang
Version:		1.4.5.1
Author URI:		
*/

register_activation_hook( __FILE__, create_function("", '$ver = "5.3"; if( version_compare(phpversion(), $ver, "<") ) die( "This plugin requires PHP version $ver or greater be installed." );') );

require __DIR__.'/index.php';
