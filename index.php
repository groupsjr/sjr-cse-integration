<?php

namespace sjr\cse;

if( file_exists(__DIR__.'/vendor/autoload.php') )
	require __DIR__.'/vendor/autoload.php';

if( is_admin() )
	require __DIR__.'/admin.php';

require __DIR__.'/functions.php';
require __DIR__.'/query.php';

/**
*
*/
function init(){
	$settings = get_settings();

	// add unique id to single pages
	add_action( 'wp_head', __NAMESPACE__.'\wp_head', 10, 2 );

	if( $settings->replace == 1 ){
		//
		add_filter( 'pre_get_posts', __NAMESPACE__.'\pre_get_posts_replace', 10, 2 );
		//
		add_filter( 'posts_search', __NAMESPACE__.'\posts_search_replace', 10, 2 );
	}

	if( $settings->append == 1 ){
		//
		add_filter( 'posts_results', __NAMESPACE__.'\posts_results_append', 0, 2 );
		add_filter( 'pre_get_posts', __NAMESPACE__.'\pre_get_posts_append', 0, 1 );
	}
}
add_action( 'init', __NAMESPACE__.'\init' );

/**
*	callback to get api data for /wp-json/sjr-cse/v1/search/
*	@param WP_REST_Request $request
*	@return array|string
*/
function api_cse_search( $request ){
	// index of key / cx pair in options-general.php?page=google-cse-settings
	$cse = $request->get_param( 'cse' );

	// 
	$page = max( 1, $request->get_param('page') );
	$per_page = $request->get_param( 'per_page' );

	// search query
	$s = $request->get_param( 's' );

	// site search
	$site = $request->get_param( 'site' );

	if( !$s ){
		return array( 'success' => FALSE,
					  'message' => 'Query param is missing!',
					  'posts' => array() );
	}

	$params = array(
		'siteSearch' => $site
	);

	$max_posts = $page * $per_page;
	$posts = get_results_all( $s, $params, $cse, $max_posts );
	
	$posts = array_slice( $posts, ($page-1) * $per_page, $per_page );

	$posts = array_map( function($item){
		$post = (object) array(
			'link' => $item->link,
			'title' => $item->title,
			'site' => $item->display_link,
			'_cached' => $item->_cached
		);

		return $post;
	}, $posts );

	$data = array(
		'success' => !empty($posts),
		'posts' => $posts,
	);
	
	return $data;
}

/**
*   callback to provide data for /wp-json/sjr-cse/v1/
*/
function register_api_route(){
	register_rest_route( 'sjr-cse/v1', 'search', array(
		'args' => array(
			'cse' => array(
				'default' => 0,
				'sanitize_callback' => 'absint'
			),
			'page' => array(
				'default' => 1,
				'sanitize_callback' => 'absint'
			),
			'per_page' => array(
				'default' => 10,
				'sanitize_callback' => 'absint'
			),
			's' => array(
				'sanitize_callback' => 'sanitize_text_field'
			),
			'site' => array(
				'sanitize_callback' => 'sanitize_text_field'
			)
		),
		'callback' => __NAMESPACE__.'\api_cse_search',
		'methods' => 'GET'
	) );
}
add_action( 'rest_api_init', __NAMESPACE__.'\register_api_route' );
