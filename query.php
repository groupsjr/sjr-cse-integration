<?php

namespace sjr\cse;

/**
*	makes call to google cse before query is run and subs params into WP_Query
*	@param WP_Query
*	@return WP_Query
*/
function pre_get_posts_replace( \WP_Query $wp_query ){
	global $pagenow, $wpdb;

	if( $wp_query->is_search() && (!is_admin() || $pagenow == 'admin-ajax.php') ){
		$search_query = stripslashes( $wp_query->query_vars['s'] );
		
		try {
			$data = get_results_all( $search_query ); 

			// store raw api result
			$wp_query->set( 'google_cse_data', $data );
		} catch( \Exception $e ){
			// cse requst failed, use wp native search
			return $wp_query;
		}

		$posts = array_map( function($item){
			$slug = array_filter( explode('/', parse_url($item->link, PHP_URL_PATH)) );
			$slug = end( $slug );
			
			$return = [
				'cse:id' => '',
				'link' => $item->link,
				'slug' => $slug
			];
			
			if( !empty($item->pagemap->metatags[0]->{'cse:id'}) ) 
				$return['cse:id'] = $item->pagemap->metatags[0]->{'cse:id'};
			
			return $return;
		}, $data );
			
		// store cse links
		$links = wp_list_pluck( $posts, 'link' );
		$wp_query->set( 'google_cse_links', $links );
		
		// query all posts in db that we find mappings to, and set params in main wp_query

		// need at least one value in these
		$cse_id__in = array_merge( array(''), array_filter(wp_list_pluck($posts, 'cse:id')) );
		$post_name__in = array_merge( array(''), array_filter(wp_list_pluck($posts, 'slug')) );
		
		$args = apply_filters( 'sjr\cse\query_ids_args', [
			'ignore_sticky_posts' => TRUE,
			'meta_query' => [
				'relation' => 'OR',
				[
					'key' => '_cse_id',
					'value' => $cse_id__in
				],
				[
					'compare' => 'NOT EXISTS',
					'key' => '_cse_id'
				]
				,
				[
					'compare' => 'EXISTS',
					'key' => '_cse_id'
				]
			],
			'posts_per_page' => -1,
			'post_name__in' => $post_name__in,
			'post_type' => 'any',
			'orderby' => 'post_name__in',
			'fields' => 'ids'
		] );

		$query = new \WP_Query( $args );

		$ids = $query->posts;

		$wp_query->query_vars['post__in'] = $ids;
		$wp_query->query_vars['orderby'] = 'post__in';
	}
	
	return $wp_query;
}

/**
*	remove wp built in text filtering for cse searches
*	@param string
*	@param WP_Query
*	@return string
*/
function posts_search_replace( $sql, \WP_Query $wp_query ){
	if( $cse = $wp_query->get('google_cse_data') ){
		$sql = '';
	}
	
	return $sql;
}

/**
*
*	@param WP_Query
*	@return WP_Query
*/
function pre_get_posts_append( \WP_Query $wp_query ){
	if( !is_admin() && $wp_query->is_search() ){
		$wp_query->set( 'posts_per_page', -1 );
	}

	return $wp_query;
}

/**
*
*	@param array
*	@param WP_Query
*	@return array
*/
function posts_results_append( $posts, \WP_Query $wp_query ){
	if( !is_admin() && $wp_query->is_search() ){
		// WP_Query:set_found_posts does not set found posts when there are 0 results
		if( !$wp_query->posts ){
			$wp_query->found_posts = 0;
		}

		//the total number of posts up to this point
		$expected_posts = 10 * max( 1, $wp_query->query_vars['paged'] );
		$posts = $wp_query->posts;
		$search_query = $wp_query->query_vars['s'];

		try {
			$posts = array_merge( $posts, get_results_all($search_query) );
		} catch( \Exception $e ){

		}
		
		$wp_query->found_posts = count( $posts );
		$wp_query->max_num_pages = ceil( $wp_query->found_posts / 10 );

		$first_post = (max( 1, $wp_query->query_vars['paged'] ) - 1) * 10;
		$posts = array_slice( $posts, $first_post, 10 );
		
		foreach( $posts as &$post ){
			if( !($post instanceof \WP_Post) ){
				$post = apply_filters( 'sjr\cse\post_appended', (object) array(
					'ID' => 0,
					'post_author' => '',
					'post_content' => '',
					'post_excerpt' => '',
					'post_title' => ''
				), $post ); 
			}
		}

		$wp_query->posts = $posts;
	}

	return $posts;
}
