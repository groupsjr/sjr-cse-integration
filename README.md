# SJR Google Custom Search Engine Integration

## Setup

Settings page: `/wp-admin/options-general.php?page=google-cse-settings`

## Key

API key generated in [Google API Manager](https://console.developers.google.com/apis/credentials).

## CX

Search Engine ID - found on [https://cse.google.com/cse/](https://cse.google.com/cse/) under `Basic > Details > Search Engine ID`.

## Add Config 

Allows multiple CSEs to be added. The `api`, `get_results_raw` and `get_results_all` functions can be called with zero-indexed CSE as `$config_id` parameter.

## Replace

Replace WordPress search results with API result from Google.

## Append

Append the API result from Google to the end of WordPress native search results.
