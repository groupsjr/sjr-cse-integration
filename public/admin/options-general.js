jQuery( document ).ready( function($){
	'use strict';
	
	// add user pass on site password protection
	$( 'button#add_config' ).click( function(e){
		var $item = $( '.cse-config' ).last();
		var $template = $item.clone( true );

		$template.find( 'input' ).increment_index().val( '' );

		$item.after( $template );

		e.preventDefault();
	} );

	// find input names matching [number] and increment by 1
	$.fn.increment_index = function(){
		this.each( function(i, item){
			var name = item.name;
			var index = name.match( /[0-9+]/ );

			index = parseInt( index, 10 ) + 1 ;
			name = name.replace( /\[[0-9+]\]/, '['+index+']' );
			item.name = name;
		} );

		return this;
	};
} );