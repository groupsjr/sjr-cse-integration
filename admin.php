<?php

namespace sjr\cse;

/**
*	show link to admin options in 'Settings' sidebar
*/
function admin_menu(){
	add_options_page( 'Google CSE Settings', 'Google CSE Settings', 'manage_options', 
					  'google-cse-settings', __NAMESPACE__.'\options_general' );
}
add_action( 'admin_menu', __NAMESPACE__.'\admin_menu' );

/**
*	add 'Settings' link in main plugins page
*	attached to plugin_action_links_* action
*	@param array
*	@return array
*/
function admin_plugins( $links ){
	$settings_link = '<a href="options-general.php?page=google-cse-settings">Settings</a>';  
	array_unshift( $links, $settings_link );
	
	return $links;
}
add_filter( 'plugin_action_links_sjr-google-cse/_plugin.php', __NAMESPACE__.'\admin_plugins' );

/**
*	callback for add_options_page() to render options page in admin 
*/
function options_general(){
	wp_enqueue_script( 'sjr-cse-settings', plugins_url( 'public/admin/options-general.js', __FILE__ ), 
                        array('jquery'), '' );


	// update settings
	if( wp_verify_nonce(filter_input(INPUT_POST, '_wpnonce'), 'google-cse-settings') ){
		// validate input
		$settings = filter_input_array( INPUT_POST, array(
			'google-cse-settings' => array(
				'filter' => FILTER_CALLBACK,
				'options' => function( $val ){
					return trim( $val );
				}
			)
		) );

		update_option( 'google-cse-settings', $settings['google-cse-settings'] );
	}
	
	$vars = get_settings();

	echo render( 'admin/options-general', $vars );
}

/**
*	render a page into wherever
*	@param string filename inside /views/ directory, no trailing .php
*	@param object|array variables available to view
*	@return string html
*/
function render( $template, $vars = array() ){
	return \sjr\render_theme_instance( __DIR__, __NAMESPACE__ )->process( $template, $vars );
}

/**
*	creates cse:id value for post meta tag on post save
*	@param int
*	@param WP_Post
*/
function save_post( $post_id, \WP_Post $wp_post ){
	if( !get_cse_id($post_id) )
		generate_cse_id( $post_id );
}
add_action( 'save_post', __NAMESPACE__.'\save_post', 10, 2 );
