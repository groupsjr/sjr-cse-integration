<div class="wrap">
	<h2>Google CSE Settings</h2>
	<pre><?php echo $version; ?></pre>

	<form method="post">
		<?php wp_nonce_field( 'google-cse-settings' ); ?>
		
		<?php foreach( $configs as $index => $config ): ?>
		<table class="form-table cse-config">
			<tbody>
				<tr>
					<th><label>Key:</label></th>
					<td><input name="google-cse-settings[configs][<?php echo esc_attr( $index ); ?>][key]" type="text" value="<?php echo esc_attr( $config['key'] ); ?>" class="regular-text ltr"/></td>
				</tr>
				<tr>
					<th><label>CX:</label></th>
					<td><input name="google-cse-settings[configs][<?php echo esc_attr( $index ); ?>][cx]" type="text" value="<?php echo esc_attr( $config['cx']  ); ?>" class="regular-text ltr"/></td>
				</tr>
			</tbody>
		</table>
		<?php endforeach; ?>

		<button id="add_config">Add Config</button>

		<table class="form-table">
			<tbody>
				<tr>
					<th><label>Replace:</label></th>
					<td><input name="google-cse-settings[replace]" type="checkbox" value="1" <?php checked( 1, $replace ); ?> class="regular-text ltr"/></td>
				</tr>
				<tr>
					<th><label>Append:</label></th>
					<td><input name="google-cse-settings[append]" type="checkbox" value="1" <?php checked( 1, $append ); ?> class="regular-text ltr"/></td>
				</tr>
			</tbody>
		</table>
		
		<input type="submit" value="Save Changes" class="button button-primary" id="submit" name="submit">
	</form>
</div>
